# README #

This container is a fork of https://github.com/mendsley/docker-alpine-beanstalk.

However, it has been enhanced in the following ways:

* The base has been explicitly set to alpine:3.4
* Auto-start has been added.
* Optional persistent storage ability has been added (2016-09-13 09:27).

**Using the container**

To run, simply execute

`
docker run -p 11300:11300 -ti bevand10/alpine-beanstalk
`

This will start the container and produce something along the lines of

```
#!shell
docker run -p 11300:11300 -ti bevand10/alpine-beanstalk
Unable to find image 'bevand10/alpine-beanstalk:latest' locally
latest: Pulling from bevand10/alpine-beanstalk
e110a4a17941: Already exists 
6e9ee5139deb: Pull complete 
50058da20b54: Pull complete 
629322a88063: Pull complete 
Digest: sha256:5664056b9b7dddcbede26e34a049074c58797e4e2fd6a50cae18b3857b4133c2
Status: Downloaded newer image for bevand10/alpine-beanstalk:latest
PID   USER     TIME   COMMAND
    1 root       0:00 {run.sh} /bin/sh /run.sh
    7 root       0:00 /opt/beanstalk/beanstalkd
    8 root       0:00 ps 7
Waiting for SIGTERM
```

The container stays alive until it is stopped with either `docker stop` or `docker kill`.

**Verbose mode**

To start with more diagnostics, override the std run command as follows:

`
docker run -p 11300:11300 -ti bevand10/alpine-beanstalk /run.sh -V
`

For more verbosity, add another V.

**Persistent Storage mode**

To persist beanstalkd queue data, mount a VOLUME when starting your container -
either a local directory or link to a volume in another container. Your volume
**MUST** be mounted as `/binlog` in your docker container.

For example, using `/home/myaccount/beanstore` as the target
*persist my beanstalkd data here* folder, start your docker as follows:

`
docker run -p 11300:11300 -v /home/myaccount/beanstore:/binlog -ti bevand10/alpine-beanstalk
`

The startup script (run.sh) detects the presence of the /binlog mount and adds the
appropriate startup argument (-b /binlog) to the beanstalkd execute command.
