#!/bin/sh

# Written: Dave Bevan bitbucket.org/bevand10

#
# As the cmd/entrypoint in docker runs as pid=1, it will not react to receiving
# SIGTERM (sent when docker stop is issued).
#
# We need to background the target process (beanstalkd in this case), arrange to
# catch SIGTERM ourselves, and forward that, when received, to the target.
#

target=/opt/beanstalk/beanstalkd

# basic prep
pid=0

# our trap forwarder
forwardTERM() {
	if [ $pid -ne 0 ]
	then
		echo "[INFO] Forward SIGTERM to $pid"
		kill -SIGTERM $pid
		wait $pid
	fi
	exit 0
}

# enable the signal handler
trap "forwardTERM" TERM

#
# Auto-detect external mount of /binlog.
# If present, launch in persist mode, using /binlog as the target directory.
#
binlog=""
if [ $(mount | grep -c /binlog) -gt 0 ]
then
	echo "[INFO] Start beanstalkd with persistant storage enabled: /binlog"
	binlog="-b /binlog"
fi

# launch the target as a background process - including any supplied arguments
$target $binlog $@ &

# catch its pid
pid=$!

# show processes at play
ps $pid

# And finally wait. Waiting ensures that "this" process - the shell - does not exit
# prematurely. If we didn't wait the container would stop immediately as there's
# nothing to keep it occupied.
echo "[INFO] Waiting for SIGTERM"
wait $pid
